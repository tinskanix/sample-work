import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { AuthenticationService } from "../_services/authentication.service";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {

  constructor(private auth: AuthenticationService, private router: Router) { }

  join() {
    this.router.navigate(["register"]);
  }

  ngOnInit() {
    // check if user was logged in before and redirect,
    // if session is invalid, handler for UnauthenticatedError will
    // redirect the user to the login page
    this.auth.currentUser.subscribe((user) => {
      if(user !== null) {
        this.router.navigate(['/home-tabs/activities']);
      } else {
        // console.log('no user in storage');
      }
    });
  }
}
