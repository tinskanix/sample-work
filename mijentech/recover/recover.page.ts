import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl, ValidatorFn  } from '@angular/forms';
import { Apollo } from "apollo-angular";
import gql from "graphql-tag";
import { ToastController} from "@ionic/angular";
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser/ngx";
import { AuthenticationService } from "../_services/authentication.service";

// Define mutator
const recoverMutation = gql`
mutation recoverPassword($email: String) {
  recoverPassword(email: $email)
}`;
@Component({
  selector: 'app-recover',
  templateUrl: './recover.page.html',
  styleUrls: ['./recover.page.scss'],
})

export class RecoverPage implements OnInit {
   private recoverForm : FormGroup;

  attemptSubmit(){
   this.apollo.mutate({
     mutation: recoverMutation,
     variables: {
       email : this.recoverForm.value.emailAddress
     }
   }).subscribe(({ data }) => {
     if(data.recoverPassword == false){
       console.log('there was an error with recoverPassword method')
     }
     this.presentDefaultToast();
   }, (Error) => {
     console.log('there was an error sending recover query', Error);
   });
  }

  constructor(
     private formBuilder : FormBuilder,
     private apollo: Apollo,
     public toastController: ToastController,
     private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.recoverForm = this.formBuilder.group({
       emailAddress: new FormControl('', Validators.compose([
         Validators.required,
         Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
       ]))
    });
  }

  async presentDefaultToast(){
    const toast = await this.toastController.create({
      message: 'If email exists in our database, you will receive an email address with a recovery link',
      showCloseButton: true,
      position: 'middle',
      closeButtonText: 'Ok'
    });
    toast.present();
   }
}
