
/**
 *  Schema types definitions
 */

/**
 * User Accounts
 */

type UserProfile = {
    id: number,
    first_name: string,
    last_name: string,
    mobile_number: string,
    zip_code: string,
    is_receiving_texts: boolean,
    is_latinx_chicanx: boolean,
    photo_url: string,
    bio: string,
    birth_date: Date,
    website_url: string,
    twitter_url: string,
    facebook_url: string,
    linkedin_url: string,
    user: User,
    created_at: Date,
    updated_at: Date
};

type User = {
    id: number,
    name: string,
    email: string,
    user_profile: UserProfile,
    created_at: Date,
    updated_at: Date
};

type AuthToken = {
    token_type: string,
    expires_in: number,
    access_token: string,
    refresh_token: string,
};

type LoginPayload = {
    auth_token: AuthToken
    user: User
};

/**
 * Feeds
 */

type Petition = {
    id: number,
    title: string,
    slug: string,
    url: string,
    image_url: string,
    who: string,
    what: string,
    why: string,
    category: string,
    creator_name: string,
    goal: number,
    signature_count: number,
    created_at: Date,
    updated_at: Date
};

type Event = {
    id: number,
    headline: string,
    slug: string,
    name: string,
    title: string,
    intro: string,
    status: string,
    start_time: Date,
    end_time: Date
};

export {
    UserProfile,
    User,
    AuthToken,
    LoginPayload,
    Petition,
    Event
}