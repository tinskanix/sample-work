import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { ToastController } from "@ionic/angular";
import {AuthenticationService} from "../_services/authentication.service"

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;

  openRecover(){
    this.router.navigate(["recover"]);
  }

  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
    private auth: AuthenticationService,
    private toastController: ToastController
    ) {
    this.loginForm = formBuilder.group({
      emailaddress: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    })
   }

   login(){
    // call login function
    let username: string = this.loginForm.value.emailaddress;
    let password: string = this.loginForm.value.password;

    this.auth.login(username, password, (success, error) => {
      if(error) {
        this.presentToastOnError();
      } else if (success){
        console.log('on login view we are ok.');
        this.router.navigateByUrl('/home-tabs/activities');
      }
    });
  }

  async presentToastOnError() {
    const toast = await this.toastController.create({
      message: 'Incorrect Credentials, Try Again',
      position: 'middle',
      duration: 1500
    });
    toast.present();
  }

  ngOnInit() {
  }

}
