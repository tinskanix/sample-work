import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController, Platform } from "@ionic/angular";
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser/ngx";
import { Apollo } from "apollo-angular";
import gql from "graphql-tag";

// Define mutator
const registerMutation = gql`
mutation registerUser($name: String!, $email: String!, $password: String!) {
  registerUser(name: $name, email: $email, password: $password)
}
`;

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public registerForm: FormGroup;

  login(){
    this.router.navigate(["login"]);
  }

  terms() {
    this.router.navigate(["terms"]);
  }

  constructor(
    private router: Router,
    private apollo: Apollo,
    private inAppBrowser: InAppBrowser,
    private platform: Platform,
    public formBuilder: FormBuilder,
    public toastController: ToastController) {
    this.registerForm = formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      // verifiedpassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    })
  }

  attemptSubmit(){
    this.apollo.mutate({
      mutation: registerMutation,
      variables: {
        name: this.registerForm.value.name,
        email: this.registerForm.value.email,
        password: this.registerForm.value.password
      }
    }).subscribe(({ data }) => {
      if(data.registerUser === true) {
        this.presentToastOnSuccess();
        this.router.navigate(['/login', this.registerForm.value]);
      } else {
        this.presentToastOnError();
      }
    },(error) => {
      console.log('there was an error sending the query', error);
      this.presentToastOnError();
    });
  }
  /* // not in use right now
  handleOpenUrl(url: string) {
    const androidOptions: InAppBrowserOptions = {
      location: 'no',
      hardwareback: 'yes',
      hideurlbar: 'yes'
    };
    const iosOptions: InAppBrowserOptions = {
      zoom: 'no',
      location: 'yes',
      toolbar: 'yes',
      clearcache: 'yes',
       clearsessioncache: 'yes',
       disallowoverscroll: 'yes',
       enableViewportScale: 'yes'
    };
    if(this.platform.is('ios')){
      const browser = this.inAppBrowser.create(url, '_self', iosOptions);
    } else{
      const browser = this.inAppBrowser.create(url, '_self', androidOptions);
    }
  }
*/
  async presentToastOnSuccess() {
    const toast = await this.toastController.create({
      message: 'Nice, now log in with your new account!',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }

  async presentToastOnError() {
    const toast = await this.toastController.create({
      message: 'An error occurred while registering, check your information and try again',
      showCloseButton: true,
      position: 'middle',
      closeButtonText: 'Ok'
    });
    toast.present();
  }

  ngOnInit() {
  }

}
