ko.oneTimeDirtyFlag = function (root) {
            var _initialized;

            //one-time dirty flag that gives up its dependencies on first change
            var result = ko.computed(function () {
                if (!_initialized) {
                    //just for subscriptions
                    ko.toJS(root);

                    //next time return true and avoid ko.toJS
                    _initialized = true;

                    //on initialization this flag is not dirty
                    return false;
                }

                //on subsequent changes, flag is now dirty
                return true;
            });

            return result;
        };

        ko.dirtyFlag = function(root, isInitiallyDirty) {
            var result = function() {},
                _initialState = ko.observable(ko.toJSON(root)),
                _isInitiallyDirty = ko.observable(isInitiallyDirty);

            result.isDirty = ko.computed(function() {
                return _isInitiallyDirty() || _initialState() !== ko.toJSON(root);
            });

            result.reset = function() {
                _initialState(ko.toJSON(root));
                _isInitiallyDirty(false);
            };

            return result;
        };

        ko.extenders.required = function (target, object) {
            target.hasError = ko.observable();
            target.validationMessage = ko.observable();

            //define function to do validation
            function validate(newValue) {
                if (object.IsSelected()) {
                    target.hasError(/\S/.test(newValue) ? false : true);
                    target.validationMessage(/\S/.test(newValue)  ? "" : "*");
                } else {
                    target.hasError(false);
                }
            };

            // initial validation
            validate(target());
            //validate whenever the value changes
            target.subscribe(validate);
            //return the original observable
            return target;
        };
        
        var HealthCode = function (data) {
            var self = this;
            self.ProfileId = ko.observable(data.ProfileId);
            self.HealthCodeAppId = ko.observable(data.HealthCodeAppId);
            self.TypeId = ko.observable(data.TypeId);
            self.HealthCodeValue = ko.observable(data.HealthCodeValue.trim());
            self.HealthCodeAppId = ko.observable(data.HealthCodeAppId);
            self.PersonId = ko.observable(0);
            self.Description = ko.observable(data.Description);
            self.TypeDescription = ko.observable(data.TypeDescription);
            self.IsSelected = ko.observable(data.IsSelected);
            self.IsRequired = ko.observable(data.IsRequired);
            self.ElaborationRequired = ko.observable(data.ElaborationRequired);
            if (self.ElaborationRequired()) {
                // Long Comment for questions where elaboration is required if selected
                self.LongComment = ko.observable(data.LongComment).extend({ required: self });
            } else {
                self.Comment = ko.observable(data.Comment);
            };

            self.serializedValue = function () {
                var serializedValue = ko.toJSON(self);
                return serializedValue;
            };
            self.dirtyFlag = new ko.dirtyFlag(self);
            self.IsChanged = ko.observable(data.IsChanged);


            // validating "Comment" only when this healthCode is checked
            self.IsSelected.subscribe(function (newValue) {
                if (/\S/.test(newValue) && self.compare) {
                    self.LongComment.hasError(false);
                } else if(self.LongComment !== undefined){
                    self.LongComment.hasError(true);
                    // manual error display since KO doesnt update error msg
                    // based on observable above
                    var textBoxSelector = "#" + self.HealthCodeValue();
                    $(textBoxSelector).text("*");
                    $(textBoxSelector).show();
                }
            }, self);
        }

        var HealthCodeGroup = function (name, healthCodes) {
            var self = this;
            self.TypeDescription = name;
            self.HealthCodeRow = ko.computed(function () {
                var result = new Array();
                var row = ko.observableArray();
                var colLength = parseInt(2);
                for (var i = 0; i < healthCodes.length; i++) {
                    if (healthCodes[i].TypeId() === "I") {
                        if (healthCodes[i].HealthCodeValue().trim() !== 'TETANUS') {
                            continue;
                        }
                    }

                    if (i % colLength === 0 && row.length > 0) {
                        result.push(row);
                        row = [];
                    }
                    if (healthCodes[i].HealthCodeValue().trim() !== 'DRUGNKA') {
                        row.push(healthCodes[i]);
                    }
                    if (i === healthCodes.length - 1) {
                        result.push(row);
                    }
                }
                return result;
            });
        }

        function compare(a, b) {
            if (a.TypeDescription < b.TypeDescription)
                return -1;
            if (a.TypeDescription > b.TypeDescription)
                return 1;
            return 0;
        };

        var HealthCodesViewModel = function (initialData) {
            var self = this;
            self.list = ko.observableArray(initialData);

            self.typeCategories = function () {
                var typeIds = ko.utils.arrayMap(this.list(), function (item) {
                    return item.TypeId();
                });
                var typeDescriptions = ko.utils.arrayMap(this.list(), function (item) {
                    return item.TypeDescription();
                });

                var distinctTypeIds = ko.utils.arrayGetDistinctValues(typeIds);
                var distinctTypeDesc = ko.utils.arrayGetDistinctValues(typeDescriptions);

                var categories = new Array();
                for (var i = 0; i < distinctTypeIds.length; i++) {
                    if (distinctTypeIds[i] !== "D") {
                        categories.push({ TypeId: distinctTypeIds[i], TypeDescription: distinctTypeDesc[i] });
                    }
                };
                categories.sort(compare);
                return categories;
            };

            self.groupedHealthCodes = ko.computed(function () {
                var typeCategories = self.typeCategories();
                var tempArray = new Array();
                for (var i = 0; i < typeCategories.length; i++) {
                    var tempGroup = new HealthCodeGroup(typeCategories[i].TypeDescription,
                        ko.utils.arrayFilter(self.list(), function (healthCode) {
                            return healthCode.TypeId() === typeCategories[i].TypeId;
                        }));
                    if (tempGroup.HealthCodeRow().length > 0) {
                        tempArray.push(tempGroup);
                    }
                };
                return tempArray;
            });

            self.getDirtyItems = ko.computed(function() {
                var selected = new Array();
                for (var i = 0; i < self.groupedHealthCodes().length; i++) {
                    for (var j = 0; j < self.groupedHealthCodes()[i].HealthCodeRow().length; j++) {
                        for (var k = 0; k < self.groupedHealthCodes()[i].HealthCodeRow()[j]().length; k++) {
                            if (self.groupedHealthCodes()[i].HealthCodeRow()[j]()[k].dirtyFlag.isDirty()) {
                                var selectedCode = self.groupedHealthCodes()[i].HealthCodeRow()[j]()[k];
                                selected.push(selectedCode);
                            }
                        }
                    }
                }
                return selected;
            }, self);
    
            self.isDirty = ko.computed(function() {
                return self.getDirtyItems().length > 0;
            }, self);

            self.saveUrl = "#";

            self.submitCodes = function () {
                var serializedHealthInfo = new Array();

                var selectedItems = self.getDirtyItems();
                var alertMessage = "";
                var errors = 0;
                ko.utils.arrayForEach(selectedItems, function (item) {
                    if (item.dirtyFlag.isDirty()) {
                        item.IsChanged = true;
                        serializedHealthInfo.push(item.serializedValue());
                        if (typeof item.LongComment !== "undefined") {
                            if (item.IsSelected() && (item.LongComment.hasError() || item.LongComment() === "")) {
                                alertMessage += " " + item.Description() + ", ";
                                errors++;
                            }
                        }
                    }
                });
                alertMessage = alertMessage.slice(0, -2);
                if (errors > 0) {
                    pushMessage("Please elaborate for item(s):  <span style='font-weight:bold'>" + alertMessage + "</span> before submitting.", "warning");
                    return;
                }

                <!-- var dataJson = JSON.stringify({ profilesJson: serializedHealthInfo, encUserId: "<%= this.EncryptedId %>" }); -->
                <!-- $("i.fa.fa-spinner").show(); -->
                <!-- $("i.fa.fa-spinner").toggleClass('fa-spin'); -->
                
                <!-- $.ajax({ -->
                        <!-- data: dataJson, -->
                        <!-- type: "POST", -->
                        <!-- dataType: "json", -->
                        <!-- contentType: "application/json; charset=utf-8", -->
                        <!-- url: self.saveUrl -->
                    <!-- }) -->
                    <!-- .done(function () { -->
                        <!-- pushMessage('Your health information was succesfully saved.', 'success'); -->
                    <!-- }) -->
                    <!-- .fail(function () { -->
                        <!-- pushMessage('Failed to save your health information.', 'danger'); -->
                    <!-- }) -->
                    <!-- .always(function () { -->
                        <!-- $("i.fa.fa-spinner").hide(); -->
                        <!-- $("i.fa.fa-spinner").toggleClass('fa-spin'); -->
                        <!-- $('#paceTracking').append('<i class="pace-loadComplete"></i>'); -->
                        <!-- $("#scroll-to-top").click(); -->
                    <!-- }); -->
            };
            self.TypeOfForm = "<%= this.TypeOfForm %>";
        }

        function pushMessage(message, priority) {
            var alertHtml = '<div class="alert alert-' + priority +
                ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + message + '</div>';
            $('#alertsBox').append(alertHtml).alert();
            $('#alertsBoxBottom').append(alertHtml).alert();
        }
		json = [
  {
    "Comment": null,
    "Description": "Adenoidectomy",
    "ElaborationRequired": true,
    "HealthCodeAppId": 1,
    "HealthCodeValue": "ADENOID",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Surgery",
    "TypeId": "Y"
  },
  {
    "Comment": null,
    "Description": "Alcohol/Beer",
    "ElaborationRequired": false,
    "HealthCodeAppId": 2,
    "HealthCodeValue": "ALCOHOL",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Social History/Habits",
    "TypeId": "S"
  },
  {
    "Comment": null,
    "Description": "Allergies/Hay Fever",
    "ElaborationRequired": false,
    "HealthCodeAppId": 3,
    "HealthCodeValue": "ALLERGY",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Condition",
    "TypeId": "C"
  },
  {
    "Comment": null,
    "Description": "Anemia",
    "ElaborationRequired": false,
    "HealthCodeAppId": 4,
    "HealthCodeValue": "ANEMIA",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Condition",
    "TypeId": "C"
  },
  {
    "Comment": null,
    "Description": "Appendectomy",
    "ElaborationRequired": true,
    "HealthCodeAppId": 6,
    "HealthCodeValue": "APPENDEC",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Surgery",
    "TypeId": "Y"
  },
  {
    "Comment": null,
    "Description": "Arthritis",
    "ElaborationRequired": false,
    "HealthCodeAppId": 7,
    "HealthCodeValue": "ARTHRIT",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Condition",
    "TypeId": "C"
  },
  {
    "Comment": null,
    "Description": "Arthritis - Family",
    "ElaborationRequired": false,
    "HealthCodeAppId": 8,
    "HealthCodeValue": "ARTHRITF",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Family History",
    "TypeId": "F"
  },
  {
    "Comment": null,
    "Description": "Aspirin Allergy",
    "ElaborationRequired": false,
    "HealthCodeAppId": 9,
    "HealthCodeValue": "ASPIRIN",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Drug Allergies",
    "TypeId": "A"
  },
  {
    "Comment": null,
    "Description": "Asthma",
    "ElaborationRequired": false,
    "HealthCodeAppId": 11,
    "HealthCodeValue": "ASTHMAC",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Condition",
    "TypeId": "C"
  },
  {
    "Comment": null,
    "Description": "Asthma - Family",
    "ElaborationRequired": false,
    "HealthCodeAppId": 10,
    "HealthCodeValue": "ASTHMA",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Family History",
    "TypeId": "F"
  },
  {
    "Comment": null,
    "Description": "Bee Sting Allergy",
    "ElaborationRequired": false,
    "HealthCodeAppId": 93,
    "HealthCodeValue": "INSECT",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Environmental Allergies",
    "TypeId": "E"
  },
  {
    "Comment": null,
    "Description": "Birth Control",
    "ElaborationRequired": false,
    "HealthCodeAppId": 117,
    "HealthCodeValue": "MED5",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Medications",
    "TypeId": "M"
  },
  {
    "Comment": null,
    "Description": "Blood Disease",
    "ElaborationRequired": false,
    "HealthCodeAppId": 14,
    "HealthCodeValue": "BLOOD",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Condition",
    "TypeId": "C"
  },
  {
    "Comment": null,
    "Description": "Blood Disease - Family",
    "ElaborationRequired": false,
    "HealthCodeAppId": 15,
    "HealthCodeValue": "BLOODDZ",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Family History",
    "TypeId": "F"
  },
  {
    "Comment": null,
    "Description": "Blood Transfusion",
    "ElaborationRequired": false,
    "HealthCodeAppId": 13,
    "HealthCodeValue": "BLDTRANS",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Condition",
    "TypeId": "C"
  },
  {
    "Comment": null,
    "Description": "Breast Disease",
    "ElaborationRequired": false,
    "HealthCodeAppId": 16,
    "HealthCodeValue": "BREASTDZ",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Condition",
    "TypeId": "C"
  },
  {
    "Comment": null,
    "Description": "Bronchitis",
    "ElaborationRequired": false,
    "HealthCodeAppId": 17,
    "HealthCodeValue": "BRONCHI",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Condition",
    "TypeId": "C"
  },
  {
    "Comment": null,
    "Description": "Cancer",
    "ElaborationRequired": false,
    "HealthCodeAppId": 19,
    "HealthCodeValue": "CANCER",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Condition",
    "TypeId": "C"
  },
  {
    "Comment": null,
    "Description": "Cancer - Family",
    "ElaborationRequired": false,
    "HealthCodeAppId": 20,
    "HealthCodeValue": "CANCERF",
    "IncludeInProfileReport": false,
    "IsChanged": false,
    "IsRequired": false,
    "IsSelected": false,
    "LastEdited": "0001-01-01T00:00:00",
    "LastEditor": null,
    "LastEditProcess": null,
    "LongComment": null,
    "PersonId": 310586,
    "ProfileId": 0,
    "SessionCode": null,
    "TypeDescription": "Family History",
    "TypeId": "F"
  }
];
        var initialData = $.map(json, function (item) { return new HealthCode(item); });
                ko.applyBindings(new HealthCodesViewModel(initialData));
				 $("#loading-indicator").find('i').toggle("fa-spin");
                $(".panel-default").show();
        //var url = "<%= GetBaseUrl() %>/services/HealthQuestionnaireService.asmx/GetHealthCodes";
        //var userString = "<%= this.EncryptedId %>";
        /*$.getJSON(url, { encUserId: userString }, function (data) {
                $("#loading-indicator").find('i').toggle("fa-spin");
                $(".panel-default").show();
            })
            .done(function (json) {
            })
            .fail(function (jqxhr, textStatus, error) {
                if (error === "Forbidden") {
                    pushMessage("<h4>Your session has expired.</h4><p>Please login again.</p>", "danger");
                    $('#task-list-container').toggle();
                }
            })
            .always(function () {
                $('#paceTracking').append('<i class="pace-loadComplete"></i>');
            });*/