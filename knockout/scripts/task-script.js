paceOptions = {
       elements: {
           selectors: ['.pace-loadComplete']
       }
   };

   function AssignedPortalTask(data) {
       var self = this;

       data = data || {};

       self.Id = ko.observable(data.Id);
       self.Title = ko.observable(data.Title);
       self.Message = ko.observable(data.Message);;
       self.StartDate = ko.observable(fixOffset(new Date(data.StartDate)));
       self.EndDate = ko.observable(fixOffset(new Date(data.EndDate)));
       self.ExpiredDate = ko.observable(fixOffset(new Date(data.ExpiredDate)));
       self.ActionName = ko.observable(data.ActionName);
       self.ActionValue = ko.observable(data.ActionValue);
       self.StatusId = ko.observable(data.StatusId);
       self.IsComplete = ko.observable(data.IsComplete);
       self.IsStarted = ko.observable(data.IsStarted);
       self.IsOverdue = ko.observable(data.IsOverdue);
       self.IsExpired = ko.observable(data.IsExpired);
       self.IsInProgress = ko.observable(data.IsInProgress);
       self.StatusName = ko.observable(data.StatusName);
   }

   function fixOffset(date) {
       var d = new Date();
       var n = d.getTimezoneOffset();

       date.setMinutes(date.getMinutes() + n);

       return date;
   }

   var PortalTaskListViewModel = function (initialData) {
       var self = this;
       window.viewModel = self;

       initialData = initialData || {};

       self.list = ko.observableArray(initialData);
       self.isReviewActive = ko.observable(false);
       self.reviewActiveText = ko.observable("Review Off");

       self.filters = new Array('incomplete', 'complete', 'overdue', 'upcoming');
       self.taskFilter = ko.observable('');
       self.SetFilter = function (taskFilter) {
           if (self.taskFilter() !== taskFilter || self.taskFilter() === '') {
               self.taskFilter(taskFilter);
           } else {
               self.taskFilter('');
           }
       };

       self.filteredList = ko.computed(function () {
           if (self.taskFilter() === '') {
               return self.list();
           }
           else {
               if (self.taskFilter() === self.filters[0]) {
                   return ko.utils.arrayFilter(self.list(), function (task) {
                       return !task.IsComplete() && task.IsStarted() && !task.IsOverdue();
                   });
               } else if (self.taskFilter() === self.filters[1]) {
                   return ko.utils.arrayFilter(self.list(), function (task) {
                       return task.IsComplete();
                   });
               } else if (self.taskFilter() === self.filters[2]) {
                   return ko.utils.arrayFilter(self.list(), function (task) {
                       return task.IsOverdue() && task.IsStarted() && !task.IsComplete();
                   });
               } else if (self.taskFilter() === self.filters[3]) {
                   return ko.utils.arrayFilter(self.list(), function (task) {
                       return !task.IsStarted() && !task.IsComplete();
                   });
               }

               return self.list();
           }
       });


       self.completeTasks = ko.computed(function () {
           var count = 0;
           for (var i = 0; i < self.list().length; i++) {
               var item = self.list()[i];
               if (item.IsComplete()) {
                   count++;
               }
           }
           return count;
       });

       self.incompleteTasks = ko.computed(function () {
           var count = 0;
           for (var i = 0; i < self.list().length; i++) {
               var item = self.list()[i];
               if (!item.IsComplete() && item.IsStarted() && !item.IsOverdue() && item.StatusId() !== 4) {
                   count++;
               }
           }
           return count;
       });


       self.reviewTasks = ko.computed(function () {
           var count = 0;
           for (var i = 0; i < self.list().length; i++) {
               var item = self.list()[i];
               if (item.StatusId() === 4) {
                   count++;
               }
           }
           return count;
       });

       self.upcomingTasks = ko.computed(function () {
           var count = 0;
           for (var i = 0; i < self.list().length; i++) {
               var item = self.list()[i];
               if (!item.IsStarted()) {
                   count++;
               }
           }
           return count;
       });

       self.overdueTasks = ko.computed(function () {
           var count = 0;
           for (var i = 0; i < self.list().length; i++) {
               var item = self.list()[i];
               if (item.IsOverdue() && item.IsStarted() && !item.IsComplete()) {
                   count++;
               }
           }
           return count;
       });

       self.isEmptyList = ko.computed(function() {
           return self.list().length === 0 || self.list().contains;
       });

       self.performAction = function (task) {
           var actionUri = task.ActionValue();
           window.open(actionUri, 'actionWindow', '');
       };

       self.cancel = function () {
           self.selectedItem(null);
       };
   };

   function pushMessage(message, priority) {
       $('#alertsBox').append('<div class="alert alert-' + priority +
           ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + message + '</div>').alert();
   }

   // LOAD tasks from server

   json = [
     {
       ActionId: 389,
       ActionName: "Your Email",
       ActionValue: "/campusweb/checkYourMail",
       ActionTypeId: 1,
       ActionTypeName: "URL",
       EndDate: "2019-06-13T20:00:00",
       Active: true,
       Id: 357,
       Message:
         "Fall 2019 - Learn how to access your Yale Email and view and accept the College's Code for Computer Resource Use.",
       StartDate: "2019-05-16T00:00:00",
       ExpirationDate: "2019-10-01T20:00:00",
       Title: "Your Yale Email and the Code for Computer Resource Use ",
       LastEditor: "SetTaskProgressStatus API",
       LastEditProcess: "Task Completion",
       LastEdited: "2019-05-16T15:54:34",
       Roles: [14, 28, 81],
       IsComplete: true,
       IsOverdue: false,
       IsStarted: true,
       IsInProgress: false,
       Expired: false,
       StatusChangeDate: "2019-05-16T15:54:34",
       StatusId: 2,
       StatusName: "Complete",
       PersonId: 310586
     },
     {
       ActionId: 406,
       ActionName: "Watch Now",
       ActionValue: "/CampusWeb/welcomevideo.aspx",
       ActionTypeId: 1,
       ActionTypeName: "URL",
       EndDate: "2019-07-04T05:00:00",
       Active: true,
       Id: 374,
       Message: "Watch a special message from President Tsutstui",
       StartDate: "2019-05-16T00:00:00",
       ExpirationDate: "2019-10-02T05:00:00",
       Title: "Welcome from the President",
       LastEditor: "SetTaskProgressStatus API",
       LastEditProcess: "Task Completion",
       LastEdited: "2019-06-14T14:03:07",
       Roles: [14, 28, 81],
       IsComplete: true,
       IsOverdue: false,
       IsStarted: true,
       IsInProgress: false,
       Expired: false,
       StatusChangeDate: "2019-06-14T14:03:07",
       StatusId: 2,
       StatusName: "Complete",
       PersonId: 310586
     },
     {
       ActionId: 391,
       ActionName: "Submit Waiver",
       ActionValue: "/campusweb/health/insurancewaiver",
       ActionTypeId: 1,
       ActionTypeName: "URL",
       EndDate: "2019-06-23T05:00:00",
       Active: true,
       Id: 359,
       Message:
         "Fall 2019 - Please submit your insurance waiver so that you are not charged for health insurance you do not need.",
       StartDate: "2019-05-21T00:00:00",
       ExpirationDate: "2019-08-01T05:00:00",
       Title: "Submit Health Insurance Waiver",
       LastEditor: "SetTaskProgressStatus API",
       LastEditProcess: "Task Completion",
       LastEdited: "2019-06-04T15:32:30",
       Roles: [81, 103],
       IsComplete: true,
       IsOverdue: false,
       IsStarted: true,
       IsInProgress: false,
       Expired: false,
       StatusChangeDate: "2019-06-04T15:32:30",
       StatusId: 2,
       StatusName: "Complete",
       PersonId: 310586
     },
     {
       ActionId: 400,
       ActionName: "View Form",
       ActionValue: "/campusweb/health/tbassessment.aspx",
       ActionTypeId: 1,
       ActionTypeName: "URL",
       EndDate: "2019-06-20T05:00:00",
       Active: true,
       Id: 368,
       Message: "Submit Tuberculosis Risk Assessment Information.",
       StartDate: "2019-05-21T00:00:00",
       ExpirationDate: "2020-05-01T05:00:00",
       Title: "Tuberculosis Risk Assessment Form",
       LastEditor: "SetTaskProgressStatus API",
       LastEditProcess: "Task Completion",
       LastEdited: "2019-06-04T16:04:20",
       Roles: [14, 28, 81],
       IsComplete: false,
       IsOverdue: true,
       IsStarted: true,
       IsInProgress: false,
       Expired: false,
       StatusChangeDate: "2019-06-04T16:04:20",
       StatusId: 2,
       StatusName: "Complete",
       PersonId: 310586
     },
     {
       ActionId: 393,
       ActionName: "Review",
       ActionValue: "/campusweb/health/athletics/supplement.aspx",
       ActionTypeId: 1,
       ActionTypeName: "URL",
       EndDate: "2019-08-23T00:00:00",
       Active: true,
       Id: 361,
       Message: "Review supplemental insurance information and update as needed.",
       StartDate: "2019-05-21T00:00:00",
       ExpirationDate: "2020-05-01T00:00:00",
       Title: "Review Health Insurance Supplement for Athletes",
       LastEditor: "Beltran",
       LastEditProcess: "TaskAdministration.Save",
       LastEdited: "2019-04-22T13:41:00",
       Roles: [37, 81],
       IsComplete: false,
       IsOverdue: false,
       IsStarted: true,
       IsInProgress: true,
       Expired: false,
       StatusChangeDate: "2019-04-22T13:41:00",
       StatusId: 0,
       StatusName: "Assigned",
       PersonId: 310586
     },
     {
       ActionId: 390,
       ActionName: "Choose TEC Course Preferences",
       ActionValue: "/campusweb/engagedcitizen/selection.aspx",
       ActionTypeId: 1,
       ActionTypeName: "URL",
       EndDate: "2019-08-07T00:00:00",
       Active: true,
       Id: 358,
       Message:
         "Fall 2019 - Fill out your course preferences for 'The Engaged Citizen'",
       StartDate: "2019-07-18T00:00:00",
       ExpirationDate: "2019-07-16T00:00:00",
       Title: "Sign up for TEC ",
       LastEditor: "beltran",
       LastEditProcess: "manual",
       LastEdited: "2019-05-20T14:06:35",
       Roles: [81, 104, 105],
       IsComplete: false,
       IsOverdue: false,
       IsStarted: false,
       IsInProgress: false,
       Expired: false,
       StatusChangeDate: "2019-05-20T14:06:35",
       StatusId: 0,
       StatusName: "Assigned",
       PersonId: 310586
     }
   ];
   var initialTasks = $.map(json, function (item) {
               return new AssignedPortalTask(item);})
  ko.applyBindings(new PortalTaskListViewModel(initialTasks));

   // $.getJSON("<%= GetBaseUrl() %>/CampusWeb/Services/PortalTaskManagerService.asmx/GetAssignedTasks?userid=<%= EktronUserManager.GetJenzabarIdFromUserData(this.CurrentEktronUser) %>")
   //     .done(function (json) {
   //         json = json || {};
   //         initialTasks = $.map(json, function (item) {
   //             return new AssignedPortalTask(item);
   //         });
   //
   //         ko.applyBindings(new PortalTaskListViewModel(json));
   //     })
   //     .fail(function (jqxhr, textStatus, error) {
   //         if (error === "Forbidden") {
   //             pushMessage("<h4>Your session has expired.</h4><p>Please login again.</p>", "danger");
   //             $('#task-list-container').toggle();
   //         }
   //     })
   //     .always(function () {
   //         $('#paceTracking').append('<i class="pace-loadComplete"></i>');
   //     });
